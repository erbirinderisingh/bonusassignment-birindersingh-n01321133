﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BonusAssignment
{
    public class Number
    {
        private int x;
        public Number(int a)
        {
            x = a;
        }
        public String IsPrime()
        {
            string ans="";
            bool isP = true;
            for (int i = x-1; i > 1; i--)
            {
                if(x % i == 0)
                {
                    isP = false;
                    ans = ans + i.ToString() + ",";
                }
            }
            if (isP)
            {
                ans = "Yes, " + x + " is a Prime Number";
            }
            else {
                ans = "No, " + x + " is not Prime Number as it has " + ans + "as factors"; 
            }
            return ans;
        }
    }
}