﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="BonusAssignment.Cartesian" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="question">
            <p>Given input of two integers, create a server click function which prints a message to a blank div element of which quadrant the co-ordinate would fall. Validate your inputs to assume that x and y are non-zero (for the sake of simplicity).</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="userinput">
            <div>
                <label runat="server" for="X">Enter X</label>
                <asp:TextBox class="form-control" runat="server" Type="Integer" ID="X" placeholder="Enter X"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter X" ControlToValidate="X" ID="validatorX"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ControlToValidate="X" Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Cannot Enter 0"></asp:CompareValidator>
            </div>
            <div>
                <label runat="server" for="Y">Enter Y</label>
                <asp:TextBox class="form-control" runat="server" Type="Integer" ID="Y" placeholder="Enter Y"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Y" ControlToValidate="Y" ID="validatorY"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ControlToValidate="Y" Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Cannot Enter 0"></asp:CompareValidator>
            </div>
            <div>
                <asp:Button runat="server" ID="Submit" Text="Check" OnClick="Check"  class="btn btn-primary" />
            </div>
        </div>
        <div runat="server" class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="userresult">
            
        </div>
    </div>
</asp:Content>
