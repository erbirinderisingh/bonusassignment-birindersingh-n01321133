﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class StringProblem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
        protected void CheckPalindrome(object sender, EventArgs e)
        {
            string ans = "";
            string userinput = userInput.Text.ToLower();
            string ns = Reverse(userinput);
            if (userinput.CompareTo(ns) == 0)
            {
                ans = "<strong>"+userinput + "</strong> is a Palindrome";
            }
            else
            {
                ans = "<strong>"+userinput + "</strong> is not a Palindrome";
            }
            userresult2.InnerHtml = ans;
        }
    }
}