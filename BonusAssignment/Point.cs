﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BonusAssignment
{
    public class Point
    {
        private int x;
        private int y;
        private string quad;
        public Point(int a, int b)
        {
            x = a;
            y = b;
        }
        public String CalculateQuad()
        {
            if (x > 0 && y > 0)
            {
                quad = "First";
            }
            else if (x > 0 && y < 0)
            {
                quad = "Fourth";
            }
            else if (x < 0 && y > 0)
            {
                quad = "Second";
            }
            else if (x < 0 && y < 0)
            {
                quad = "Third";
            }
            return quad;
        }
    }
}