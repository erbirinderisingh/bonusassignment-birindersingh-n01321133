﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Divisible.aspx.cs" Inherits="BonusAssignment.Divisible" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="question">
            <p>An integer n is considered divisible if integer p can represent another integer k where the equation is p*k = n. An example is that 12 is divisible by 4 because 3*4 = 12.</p>
            <p>A prime number is divisible by only 1 and itself. A non-prime number can be divisible by a number which is between 1 and itself. An example is that 13 is only divisible by 1 (13*1 = 13) and 13 (1*13 = 13).</p>
            <p>The modulus operator is useful for determining divisibility in C#. The modulus operator takes two integers and returns the remainder when divided. (eg. 12%3 = 0, 14%3 = 2, 15%3 = 0, 3%3 = 0, 4%5 = 4).</p>
            <p>Given a positive integer input, write a server click function which prints a message to a blank div. The message will determine if a number is prime or not</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="userinput">
            <div>
                <label runat="server" for="X">Enter Number</label>
                <asp:TextBox class="form-control" runat="server" Type="Integer" ID="X" placeholder="Enter Number"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter X" ControlToValidate="X" ID="validatorX"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ControlToValidate="X" Type="Integer" Operator="NotEqual" ValueToCompare="0" ErrorMessage="Cannot Enter 0"></asp:CompareValidator>
            </div>
            <div>
                <asp:Button runat="server" ID="Submit" Text="Check" OnClick="CheckPrime"  class="btn btn-primary" />
            </div>
        </div>
        <div runat="server" class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="userresult1">
            
        </div>
    </div>
</asp:Content>
