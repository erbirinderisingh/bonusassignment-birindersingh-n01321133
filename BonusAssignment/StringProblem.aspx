﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StringProblem.aspx.cs" Inherits="BonusAssignment.StringProblem" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="question">
            <p> Write an input which checks that the input is a string of characters (regular expression ^[a-zA-Z]+$). Determine if a word is a palindrome or not.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="userinput">
            <div>
                <label runat="server" for="userInput">Enter String</label>
                <asp:TextBox class="form-control" runat="server" Type="String" ID="userInput" placeholder="Enter Number"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter valid String" ControlToValidate="userInput" ID="validatorX"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="userInput" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            </div>
            <div>
                <asp:Button runat="server" ID="Submit" Text="Check" OnClick="CheckPalindrome"  class="btn btn-primary" />
            </div>
        </div>
        <div runat="server" class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="userresult2">
            
        </div>
    </div>
   
</asp:Content>
